package entity

type Account struct {
	Id         int32       `reindex:"id"`
	Email      string      `reindex:"email"`
	FirstName  string      `reindex:"fname"`
	SecondName string      `reindex:"sname"`
	Phone      string      `reindex:"phone"`
	Sex        bool        `reindex:"sex"`
	Birth      int32       `reindex:"birth"`
	Country    string      `reindex:"country"`
	City       string      `reindex:"city"`
	Joined     uint32      `reindex:"joined_at"`
	Status     uint8       `reindex:"status"`
	Premium    bool        `reindex:"premium"`
	InterestIds []uint32   `reindex:"interest_ids"`
	Interests  []*Interest `reindex:"interests,,joined"`
	Likes      []*Like     `reindex:"likes,,joined"`


	_ struct{} `reindex:"id+email,,composite,pk"`

}

func (acc *Account) Join(field string, subitems []interface{}, context interface{}) {

	switch field {
	case "interests":
		for _, joinItem := range subitems {
			acc.Interests = append(acc.Interests, joinItem.(*Interest))
		}
		break
	case "likes":
		for _, joinItem := range subitems {
			acc.Likes = append(acc.Likes, joinItem.(*Like))
		}
	}
}

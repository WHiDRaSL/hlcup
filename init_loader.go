package main

import (
	"archive/zip"
	"bufio"
	"errors"
	"github.com/spf13/cast"
	"gitlab.netbox.ru/d.khassan/hlcup/entity"
	"gitlab.netbox.ru/d.khassan/hlcup/globals"
	"io/ioutil"
	"os"
	"strings"
)

const(
	FilesPath = "/tmp/data/data.zip"
    DataFilePrefix = "accounts_"
)
func getAccountsGenerator(accountParsedChan chan *entity.AccountsRaw) error {

	println("file unzip start")
	zipReader, err := zip.OpenReader(FilesPath)
	if err != nil {
		return err
	}
	println("file unzipped")
	for _, file := range zipReader.File {
		fileDescriptor, err := file.Open()
		if err != nil {
			return err
		}
		if file.FileInfo().IsDir() {
			return errors.New("directory inside zip")
		}

		if !strings.HasPrefix(file.FileInfo().Name(), DataFilePrefix) {
			continue
		}

		accountsJsonBytes, err := ioutil.ReadAll(fileDescriptor)
		if err != nil {
			return err
		}

		accountsParse := &entity.AccountsRaw{}
		println(len(accountsJsonBytes))
		err = accountsParse.UnmarshalJSON(accountsJsonBytes)
		if err != nil {
			return err
		}

		globals.Logger.Sugar().Infof("File %s loaded", file.FileInfo().Name())
		accountParsedChan <- accountsParse
	}

	close(accountParsedChan)

	return zipReader.Close()
}

func loadCurrentTimestamp() error {
	file, err := os.Open(OptionsFilePath)
	if err != nil {
		globals.Logger.Sugar().Error(err)

		return err
	}

	fileReader := bufio.NewReader(file)
	timestampBytes, _, err := fileReader.ReadLine()
	timestampString := cast.ToString(timestampBytes)

	globals.GlobalTimestamp = cast.ToInt32(timestampString)

	err = file.Close()
	if err != nil {
		globals.Logger.Sugar().Error(err)

		return err
	}

	return nil
}
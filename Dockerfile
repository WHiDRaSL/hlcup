FROM golang:alpine as builder

RUN apk add --no-cache git gcc musl-dev
RUN go get -a github.com/restream/reindexer
RUN sh $GOPATH/src/github.com/restream/reindexer/dependencies.sh
RUN go generate github.com/restream/reindexer/bindings/builtin

RUN go get -u github.com/golang/dep/cmd/dep

WORKDIR $GOPATH/src/gitlab.com/dennis.khassan/hlcup

APP=hlcup
TAG=latest

# Metadata about this makefile and position
MKFILE_PATH := $(lastword $(MAKEFILE_LIST))
CURRENT_DIR := $(dir $(realpath $(MKFILE_PATH)))
CURRENT_DIR := $(CURRENT_DIR:/=)

.DEFAULT_GOAL := run

DOCKER_REPO=hub.netbox.ru:5000/hlcup

build:
	docker build --compress --force-rm --pull -t "${APP}-dev:${TAG}" .

run:
	docker run --rm -it -v $(shell pwd)/data:/tmp/data:ro -v $(shell pwd):/go/src/gitlab.com/dennis.khassan/hlcup --name "${APP}-dev" "${APP}-dev:${TAG}"

build_prod:
	docker build -f Dockerfile_prod --compress --force-rm --pull -t "${APP}:${TAG}" .

push_prod: build_prod
	docker tag "${APP}:${TAG}" "${DOCKER_REPO}/${APP}:${TAG}"
	docker push "${DOCKER_REPO}/${APP}:${TAG}"

run_prod:
	docker run --rm -it -v $(shell pwd)/data:/tmp/data:ro --name "${APP}" "${APP}:${TAG}"

package main

import (
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"gitlab.netbox.ru/d.khassan/hlcup/controller"
	"gitlab.netbox.ru/d.khassan/hlcup/db"
	"gitlab.netbox.ru/d.khassan/hlcup/entity"
	"gitlab.netbox.ru/d.khassan/hlcup/globals"
)

const (
	OptionsFilePath = "/tmp/data/options.txt"
)

func init() {

	err := loadCurrentTimestamp()
	if err != nil {
		globals.Logger.Sugar().Fatal("Can't load timestamp from options.txt")
	}

	accountsChan := make(chan *entity.AccountsRaw)
	go func() {
		err := getAccountsGenerator(accountsChan)
		globals.Logger.Sugar().Error(err)
	}()

	for accountsRaw := range accountsChan {
		globals.Logger.Sugar().Error(accountsRaw)

		go db.LoadAccountsToDatabase(accountsRaw)
	}


}



func main() {
	router := initRouter()

	err := fasthttp.ListenAndServe(":80", router.Handler)
	globals.Logger.Sugar().Fatal(err)

	_ = globals.Logger.Sync()
}

func initRouter() *fasthttprouter.Router {
	router := fasthttprouter.New()
	router.GET("/accounts/filter", controller.FilterAction)
	router.GET("/accounts/group", controller.GroupAction)
	router.GET("/accounts/:id/recommend", controller.RecommendAction)
	router.GET("/accounts/:id/suggest", controller.SuggestAction)
	router.POST("/accounts/new", controller.NewAction)
	router.POST("/accounts/:id", controller.UpdateAction)
	router.POST("/accounts/likes", controller.LikesAction)

	return router
}
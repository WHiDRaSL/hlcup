package db

import (
	"github.com/restream/reindexer"
	_ "github.com/restream/reindexer/bindings/builtin"
	"gitlab.netbox.ru/d.khassan/hlcup/entity"
	"gitlab.netbox.ru/d.khassan/hlcup/globals"
)

const (
	AccountsNamespace  = "accounts"
	InterestsNamespace = "interests"
	LikesNamespace     = "likes"
)

var ReindexerDb = reindexer.NewReindex("builtin:///tmp/reindex/testdb")

func Init() error {

	err := ReindexerDb.OpenNamespace(AccountsNamespace, reindexer.DefaultNamespaceOptions(), entity.Account{})
	if err != nil {
		return err
	}

	err = ReindexerDb.OpenNamespace(InterestsNamespace, reindexer.DefaultNamespaceOptions(), entity.Account{})
	if err != nil {
		return err
	}

	err = ReindexerDb.OpenNamespace(LikesNamespace, reindexer.DefaultNamespaceOptions(), entity.Account{})
	if err != nil {
		return err
	}

	return nil

}

func LoadAccountsToDatabase(accountsRaw *entity.AccountsRaw) {
	for _, accountRaw := range accountsRaw.Accounts {

		account, interests, likes := accountRaw.FetchDataEntities()

		err := ReindexerDb.Upsert(AccountsNamespace, account)
		if err != nil {
			globals.Logger.Sugar().Error(err)
		}

		for _, interest := range interests {
			err := ReindexerDb.Upsert(InterestsNamespace, interest)
			if err != nil {
				globals.Logger.Sugar().Error(err)
			}
		}

		for _, like := range likes {
			err := ReindexerDb.Upsert(LikesNamespace, like)
			if err != nil {
				globals.Logger.Sugar().Error(err)
			}
		}
	}
}
